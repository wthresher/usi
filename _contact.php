<?php get_header(); /* Template Name: Contact */ ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();?>

    <div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page">
    	<div class="w100 clearfix wrapper-content">
        <div class="w60 left">
        	<h2><a href="mailto:info@unicornsleep.com"><img src="<?php echo get_template_directory_uri(); ?>/_inc/img/icon-email.svg" alt="Call Unicorn Sleep" /> info@unicornsleep.com</a></h2>
        </div>
        <div class="w40 left">
          <h3>Send Us a Message</h3>
          <?php echo do_shortcode( '[contact-form-7 id="90" title="Contact"]' ); ?>
        </div>
    	</div>
    </div>

  <?php endwhile; endif; ?>

<?php get_footer(); ?>