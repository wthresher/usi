<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();?>

    <div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page">
    	<div class="w100 clearfix wrapper-content">
    		<?php the_content(); ?>
    	</div>
    </div>

  <?php endwhile; endif; ?>

<?php get_footer(); ?>