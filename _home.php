<?php get_header(); /* Template Name: Home */ ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();?>
    <div id="page-<?php echo basename(get_permalink()); ?>" class="h100 relative zDex">
			<div class="w100 h100 table removeMe">
		    <div class="tableRow">
		      <div class="tableCell vam tac">
			      <div class="w100 clearfix relative pl20">
							<div id="emblem" class="clearfix relative m0a mobile-hide">
								<div id="emblem-1">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="250" height="293" viewBox="0 0 150 176" enable-background="new 0 0 150 176" xml:space="preserve">
									<path fill="none" stroke="#FFFFFF" stroke-miterlimit="10" d="M92.2,130.1l-4.8-26.6l4.2,2.3l10.7,17.7l26.5,5.3l15.8-25.5
										l-24.9-51.2L105.1,38l27.6-36.8L71.1,15.5c-0.8,0.2-17.1,4.4-32.8,15C15.9,44.2,1,68.8,1,97c0,43,34.9,77.9,77.9,77.9
										c30.9,0,57.6-18,70.2-44c-8.6,3.3-17.9,5.1-27.7,5.1c-10.3,0-20.2-2-29.2-5.7L92.2,130.1 M74.8,30.9L94,26.4l-9.9,13.1l22.4,22
										l19.9,40.9l-5.4,8.8l-8.9-1.8L102.8,94L65.7,73.8l8.8,46.4c-18.8-14.2-31-36.8-31-62.2c0-4.3,0.4-8.5,1-12.6
										C58.5,35.1,74.7,30.9,74.8,30.9z M29.3,57c0,0.3,0,0.7,0,1c0,4.5,0.3,8.9,1,13.3c-3.6-1.6-5.9-3.3-7.3-4.8
										C24.8,63.1,26.9,60,29.3,57z M18.1,77.9c3.5,2.6,8.4,5.1,15.2,7.1c1.4,4.7,3.2,9.1,5.3,13.5c-13.4-3.8-20-7.1-23.1-9.1
										C16.1,85.4,16.9,81.6,18.1,77.9z M17.6,114.5c-1.1-3.9-1.9-8-2.2-12.2c6.8,3.2,16.9,6.7,31.9,10.2c3.8,5.1,8.1,9.9,12.8,14.1
										C34.2,121.5,22.6,117.1,17.6,114.5z M78.9,160.7c-23.3,0-43.7-12.6-54.8-31.3c13.8,4.6,34.5,9.1,61.2,13.2
										c9.1,3.9,18.9,6.4,29.2,7.1C104,156.8,91.7,160.7,78.9,160.7z" class="lWAnfeoS_0"></path>
									<style>.lWAnfeoS_0{stroke-dasharray:1448 1450;stroke-dashoffset:1500;animation:lWAnfeoS_draw 2500ms ease-in-out 0ms forwards;}@keyframes lWAnfeoS_draw{100%{stroke-dashoffset:0;}}@keyframes lWAnfeoS_fade{0%{stroke-opacity:1;}94.44444444444444%{stroke-opacity:1;}100%{stroke-opacity:0;}}</style></svg>
								</div>
								<div id="emblem-2">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 width="250" height="293" viewBox="0 0 150 176" enable-background="new 0 0 150 176" xml:space="preserve">
									<g>
										<path id="usi-emblem" fill="#FFFFFF" d="M122,136.5c-10.5,0-20.4-2-29.5-5.7l-4.9-27l4.2,2.3l10.9,17.9l26.8,5.4l16-25.8l-25.3-51.8l-14.7-14.4
											l28-37.3L71,14.6c-0.8,0.2-17.3,4.4-33.3,15.2C15.1,43.6,0,68.6,0,97.1C0,140.7,35.3,176,78.9,176c31.3,0,58.3-18.2,71.1-44.6
											C141.3,134.7,131.8,136.5,122,136.5z M74.8,30.1l19.5-4.6l-10,13.3L107,61.2l20.2,41.4l-5.5,8.9l-9-1.8l-9.5-15.6L65.5,73.6l8.9,47
											C55.4,106.2,43,83.4,43,57.6c0-4.3,0.4-8.6,1-12.8C58.3,34.4,74.7,30.2,74.8,30.1z M28.7,56.6c0,0.3,0,0.7,0,1c0,4.6,0.3,9,1,13.4
											c-3.6-1.6-5.9-3.4-7.4-4.9C24.2,62.8,26.3,59.6,28.7,56.6z M17.4,77.8c3.6,2.6,8.5,5.1,15.4,7.2c1.5,4.7,3.3,9.3,5.4,13.6
											c-13.6-3.8-20.2-7.2-23.4-9.2C15.3,85.4,16.2,81.5,17.4,77.8z M16.9,114.9c-1.1-4-1.9-8.1-2.3-12.3c6.8,3.3,17.1,6.8,32.3,10.4
											c3.8,5.2,8.2,10,13,14.3C33.6,122,21.9,117.5,16.9,114.9z M78.9,161.7c-23.6,0-44.3-12.7-55.5-31.7c14,4.7,34.9,9.2,62,13.4
											c9.2,3.9,19.1,6.5,29.5,7.2C104.4,157.7,91.9,161.7,78.9,161.7z"/>
									</g>
									</svg>
								</div>
							</div>
			      </div>
		      </div>
		    </div>
			</div>
      <div id="night-trial" class="w100 tac mobile-hide">
		    <h4><strong>150</strong> Night Trial</h4>
	    </div>
			<div class="w100 h100 table revealMe vh100calc">
		    <div id="parallax-items" class="tableRow h100 w100 relative">
  		    <div class="tableCell h100 w33 vam tac parallax">
    		    <a href="<?php echo home_url(); ?>/vintage" class="button layer inline-block" data-depth="1"><span class="layer" data-depth="0.5">Vintage</span></a>
    		    <p>
    		    	Our Timeless Collection
    		    </p>
  		    </div>
  		    <div class="tableCell h100 w33 vam tac parallax">
    		    <a href="<?php echo home_url(); ?>/natural" class="button layer inline-block" data-depth="1"><span class="layer" data-depth="0.5">Natural</span></a>
    		    <p>
    		    	Our Organic Collection
    		    </p>
  		    </div>
  		    <div class="tableCell h100 w33 vam tac parallax">
    		    <a href="<?php echo home_url(); ?>/modern" class="button layer inline-block" data-depth="1"><span class="layer" data-depth="0.5">Modern</span></a>
    		    <p>
    		    	Our Contemporary Collection
    		    </p>
  		    </div>
		    </div>
		  </div>
    </div>
  <?php endwhile; endif; ?>

<svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="0" width="0">
  <defs>
     <filter id="blur" x="0" y="0">
       <feGaussianBlur stdDeviation="3" />
     </filter>
  </defs>
</svg> 

<?php get_footer(); ?>