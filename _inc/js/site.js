jQuery(document).ready(function($) {

  // Animations
  TweenLite.set('#emblem', {z:0.01}); // Set animations up
  TweenLite.defaultEase = Expo.easeOut; // Default Ease
  TweenLite.to($('.fadeToWhiteT'),5,{delay:0.25,color:"#fff",transform:"translateY(0)"});
  TweenLite.to($('.gFadeIn'),0.55,{delay:0,opacity:"1",transform:"translateY(0)"});
  TweenLite.to($('.gFadeInD1'),0.25,{delay:0.25,opacity:"1",transform:"translateY(0)"});
  TweenLite.to($('.gFadeInD2'),0.25,{delay:0.5,opacity:"1",transform:"translateY(0)"});
  TweenLite.to($('.gFadeInD3'),0.25,{delay:0.75,opacity:"1",transform:"translateY(0)"});
  TweenLite.to($('.gFadeInD4'),0.25,{delay:1,opacity:"1",transform:"translateY(0)"});
  TweenLite.to($('.gFadeInD5'),0.25,{delay:1.25,opacity:"1",transform:"translateY(0)"});

	// Emblem Animate
	TweenLite.to($('#emblem-1'),0.25,{delay:1,opacity:"0"});
	TweenLite.to($('#emblem-2'),1,{delay:1,opacity:"1"});

  // Animate Emblem on Homepage
  if($('body').hasClass('home')) {
    TweenLite.to($('body'),4,{delay:1,backgroundColor:"#253746"});
    TweenLite.to($('#header-site'),1,{delay:2,opacity:"1",transform:"translateY(0)"});
		TweenLite.to($('#footer-site'),1,{delay:2,opacity:"1",transform:"translateY(0)"});
	  TweenLite.to($('#emblem'),1,{delay:2.5,opacity:"0"});
		TweenLite.to($('#emblem'),1,{delay:2,width:"50px",transform:"translateY(-20vh)"});
		TweenLite.to($('.logo'),1,{delay:2.5,opacity:"1"});
		TweenLite.to($('.removeMe'),1,{delay:2,height:"0px"});
		TweenLite.to($('.revealMe'),1,{delay:2,opacity:"1"});
		TweenLite.to($('#night-trial'),1,{delay:3,opacity:"1"});
    setTimeout(function(){
      $('#emblem').hide();
    }, 4000);
  }

  // Current Menu Item
  if(window.location.href === "http://unicornsleep.com/us/"){
    $('#nav-us').addClass('current');
  } else if(window.location.href === "http://unicornsleep.com/inquiries/"){
    $('#nav-inquiries').addClass('current');
  } else if(window.location.href === "http://unicornsleep.com/experience/"){
    $('#nav-experience').addClass('current');
  } else if(window.location.href === "http://unicornsleep.com/vintage/"){
    $('#nav-vintage').addClass('current');
  } else if(window.location.href === "http://unicornsleep.com/natural/"){
    $('#nav-natural').addClass('current');
  } else if(window.location.href === "http://unicornsleep.com/modern/"){
    $('#nav-modern').addClass('current');
  }
  
  // Menu Link Animation
  $('#header-site,#parallax-items').on('click touch', 'a', function() {
    $('body').addClass('ohidden');
		TweenLite.to($('#header-site'),0.75,{delay:0,opacity:"0",transform:"translateY(-200px)"});
		TweenLite.to($('#footer-site'),0.25,{delay:0,opacity:"0",transform:"translateY(100px)"});
		TweenLite.to($('#particles'),0.75,{delay:0,opacity:"0",transform:"scale(5)"});
		TweenLite.to($('body,#particles'),1,{delay:0,backgroundColor:"#000"});
		TweenLite.to($('.wrapper-main'),0.35,{delay:0,opacity:"0",transform:"scale(0)"});
  });

  // wait until DOM is ready, then scale in content
  document.addEventListener("DOMContentLoaded", function(event) {
      // wait until window is loaded - meaning all images, style-sheets, and assets
      window.onload = function() {
  			TweenLite.to($('.wrapper-main'),0.35,{delay:0,opacity:"1",transform:"scale(1)"});
      };
  });

  // Parallax
  $('.parallax').parallax({
    limitX: 5,
    limitY: 5
  });
  $('.parallax-particles').parallax({
    frictionX: 0.01,
    frictionY: 0.01,
    limitX: 25,
    limitY: 15
  });

  // Modal
  $('.modal-trigger').on('click touch', function(){
    var tar = $(this).attr('rel');
    $('.modal,.modal-overlay,' + tar).removeClass('animationHide').addClass('animationShow');
  });
  $('.close-modal,.modal-overlay').on('click touch', function(){
    $('.modal,.modal-overlay').removeClass('animationShow').addClass('animationHide');
  });

  // Searching for FAQS
  var lsOptions = { valueNames: [ 'faq-title',{ data: ['sections'] } ]};
  var userList = new List('faqs', lsOptions);

  // FAQs
  $('.border-boxes').on('click touch', '.faq-section-box', function() {
    var targetSection = $(this).attr('data-targets');
    $(this).addClass('selected').siblings().removeClass('selected');
    $('#faqs-list li.'+targetSection).addClass('filtered');
    $('#faqs-list li:not(.'+targetSection+')').removeClass('filtered');
  });
  $('#faqs-list').on('click touch', 'li', function() {
    $(this).toggleClass('active').siblings().removeClass('active');
  });
  
  // Add to cart steps
  $('.product-display').each(function() {
    var productID = $(this).attr('id');
    $('#'+productID+' .steps').on('click touch', '.step-1 div', function() {
      var dataSize = $(this).attr('data-size');
      $('#'+productID+' .steps .step-2').attr('data-size',dataSize);
      TweenLite.to($('#'+productID+' .step-1'),0.25,{delay:0.25,opacity:"0",display:"none"});
      TweenLite.to($('#'+productID+' .step-2'),0.25,{delay:0.5,opacity:"1",display:"block"});
    });
    $('#'+productID+' .steps').on('click touch', '.step-2 div', function() {
      var dataSize = $('#'+productID+' .steps .step-2').attr('data-size');
      var prodID   = $('#'+productID+' .steps .step-2').attr('data-id');
      var dataFeel = $(this).attr('data-feel');
      $('#'+productID+' .steps .step-3').attr('data-feel',dataFeel);
      TweenLite.to($('#'+productID+' .step-2'),0.25,{delay:0.25,opacity:"0",display:"none"});
      TweenLite.to($('#'+productID+' .step-3'),0.25,{delay:0.5,opacity:"1",display:"block"});
    });
    $('#'+productID+' .steps').on('click touch', '.step-3 div', function() {
      var dataSize 		= $('#'+productID+' .steps .step-2').attr('data-size');
      var prodID   		= $('#'+productID+' .steps .step-2').attr('data-id');
      var dataFeel 		= $('#'+productID+' .steps .step-3').attr('data-feel');
      var dataBxs 	  = $(this).attr('data-bx');
      var variationID = $('#'+productID+' .steps .variations .variation[data-vasize='+dataSize+'][data-vafeel='+dataFeel+'][data-vbox='+dataBxs+']').attr('data-vid');
      var varPrice    = $('#'+productID+' .steps .variations .variation[data-vasize='+dataSize+'][data-vafeel='+dataFeel+'][data-vbox='+dataBxs+']').attr('data-vprice');
      $('#'+productID+' .steps .step-3').attr('data-bxs',dataBxs);
			$('#'+productID+' .step-4 h4 span:first-child').text(dataSize);
      $('#'+productID+' .step-4 h4 span:nth-of-type(2)').text(dataFeel);
      $('#'+productID+' .step-4 h4 span:nth-of-type(3)').text(dataBxs);
      $('#'+productID+' .step-4 h4 span:nth-of-type(4)').text(varPrice);
			TweenLite.to($('#'+productID+' .step-3'),0.25,{delay:0.25,opacity:"0",display:"none"});
      TweenLite.to($('#'+productID+' .step-4'),0.25,{delay:0.5,opacity:"1",display:"block"});
    });
    $('#'+productID+' .steps').on('click touch', '.step-4 a', function() {
      var dataSize 		= $('#'+productID+' .steps .step-2').attr('data-size');
      var prodID   		= $('#'+productID+' .steps .step-2').attr('data-id');
      var dataFeel 		= $('#'+productID+' .steps .step-3').attr('data-feel');
      var dataBxs 		= $('#'+productID+' .steps .step-3').attr('data-bxs');
      var variationID = $('#'+productID+' .steps .variations .variation[data-vasize='+dataSize+'][data-vafeel='+dataFeel+'][data-vbox='+dataBxs+']').attr('data-vid');
			$(this).attr('href', '/cart/?add-to-cart='+prodID+'&variation_id='+variationID+'&attribute_pa_size='+dataSize+'&attribute_pa_feel='+dataFeel+'&attribute_pa_box-spring='+dataBxs);
    });
    $('#'+productID+' .steps').on('click touch', '.step-4 .start-over', function() {
      TweenLite.to($('#'+productID+' .step-1'),0.25,{delay:0.25,opacity:"1",display:"block"});
      TweenLite.to($('#'+productID+' .step-2'),0.25,{delay:0,opacity:"0",display:"none"});
      TweenLite.to($('#'+productID+' .step-3'),0.25,{delay:0,opacity:"0",display:"none"});
      TweenLite.to($('#'+productID+' .step-4'),0.15,{delay:0,opacity:"0",display:"none"});
    });
  });
  
  // Slaask Widget
  (function() {
      var slk = document.createElement('script');
      slk.src = 'https://cdn.slaask.com/chat.js';
      slk.type = 'text/javascript';
      slk.async = 'true';
      slk.onload = slk.onreadystatechange = function() {
        var rs = this.readyState;
        if (rs && rs != 'complete' && rs != 'loaded') return;
        try {
          _slaask.init('ec69cd319162151e069b69410ef19340');
        } catch (e) {}
      };
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(slk, s);
  })();

  // Particles Background
	particlesJS("particles", {
  		"particles": {
  			"number": {
  				"value": 10,
  				"density": {
  					"enable": true,
  					"value_area": 100
  				}
  			},
  			"color": {
  				"value": "#fff"
  			},
  			"shape": {
  				"type": "circle",
  				"stroke": {
  					"width": 0,
  					"color": "#000000"
  				}
  			},
  			"opacity": {
  				"value": 1,
  				"random": true,
  				"anim": {
  					"enable": false,
  					"speed": 0.01,
  					"opacity_min": 0.5,
  					"sync": false
  				}
  			},
  			"size": {
  				"value": 2,
  				"random": true,
  				"anim": {
  					"enable": false,
  					"speed": 0.01,
  					"size_min": 0.1,
  					"sync": false
  				}
  			},
  			"line_linked": {
  				"enable": false,
  				"distance": 55,
  				"color": "#ffffff",
  				"opacity": 0.25,
  				"width": 1
  			},
  			"move": {
  				"enable": true,
  				"speed": 0.05,
  				"direction": "top-right",
  				"random": true,
  				"straight": false,
  				"out_mode": "out",
  				"bounce": false,
  				"attract": {
  					"enable": true,
  					"rotateX": 20,
  					"rotateY": 20
  				}
  			}
  		},
  		"interactivity": {
  			"detect_on": "canvas",
  			"events": {
  				"onhover": {
  					"enable": true,
  					"mode": "grab"
  				},
  				"onclick": {
  					"enable": true,
  					"mode": "repulse"
  				},
  				"resize": true
  			},
  			"modes": {
  				"grab": {
  					"distance": 100,
  					"line_linked": {
  						"opacity": 0.15
  					}
  				},
  				"bubble": {
  					"distance": 400,
  					"size": 40,
  					"duration": 2,
  					"opacity": 8,
  					"speed": 1
  				},
  				"repulse": {
  					"distance": 350,
  					"duration": 7
  				},
  				"push": {
  					"particles_nb": 4
  				},
  				"remove": {
  					"particles_nb": 2
  				}
  			}
  		},
  		"retina_detect": true
  	});

});