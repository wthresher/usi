<?php get_header(); /* Template Name: Category */ ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); $slug = get_post_field( 'post_name' ); ?>

    <div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page relative zDex">
    	<div class="w100 clearfix">
    		<div class="w70 m0a tac pt35">
    			<?php the_content(); ?>
    		</div>
				<div class="products">
					<?php
						$args = array(
							'post_type' => 'product',
							'product_cat' => $slug,
							'posts_per_page' => 12
							);
						$loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post(); $productID = get_the_id(); $pimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>

			        <div id="product-<?php the_id(); ?>" class="w100 clearfix gFadeInD2 product-display">
			        	<section>
				          <div class="w45 left pt50">
										<div id="maxx-home" data-parallax='{"y":-15,"x":20,"duration":1000}' class="dropshadow relative">
				        			<img src="<?php echo $pimage[0]; ?>" class="gFadeInD2 w90 m0a relative layer1" />
				        		</div>
				          </div>
				          <div class="wrapper-content">
					          <div class="w55 left pt55">
					          	<div data-parallax='{"y":20,"x":-70,"duration":1500}'>
						          	<h2><?php woocommerce_template_loop_product_title(); ?></h2>
                        <?php the_excerpt(); ?>
                        <div class="steps">
                          <div class="step-1">
                            <h4>Begin by selecting a size</h4>
                            <?php $sizes = wp_get_post_terms( $productID, 'pa_size', array( 'order' => 'DESC', 'orderby' => 'name')); foreach ( $sizes as $size ) { ?>
                              <div data-size="<?php echo $size->slug; ?>" class="button inline-block">
                                <?php echo $size->name; ?>
                              </div>
                            <?php } ?>
                          </div>
                          <div class="step-2" data-size="" data-id="<?php the_id(); ?>">
                            <h4>Which feel do your prefer?</h4>
                            <?php $feels = wp_get_post_terms( $productID, 'pa_feel'); foreach ( $feels as $feel ) { ?>
                              <div data-feel="<?php echo $feel->slug; ?>" class="button inline-block">
                                <?php echo $feel->name; ?>
                              </div>
                            <?php } ?>
                            <?php if( have_rows('feel_descriptions') ): ?>
                            	<ul class="fontSmall w100 clearfix pt20">
                              	<?php while( have_rows('feel_descriptions') ): the_row();
                              		$ftitle = get_sub_field('feel_type'); ?>
                              		<li class="w33 left pr20">
                                    <h4 class="m0a bold pb5"><?php echo $ftitle; ?></h4>
                                    <?php if( have_rows('feel_points') ): ?>
                                    	<ul class="disc pl15">
                                      	<?php while( have_rows('feel_points') ): the_row();
                                      		$fpoint = get_sub_field('point'); ?>
                                      		<li class="pb5">
                                      			<?php echo $fpoint; ?>
                                      		</li>
                                      	<?php endwhile; ?>
                                    	</ul>
                                    <?php endif; ?>
                              		</li>
                              	<?php endwhile; ?>
                            	</ul>
                            <?php endif; ?>
                          </div>
                          <div class="step-3" data-bxs="">
                          	<h4 class="capitalize pb5">Do you need a box spring?</h4>
														<?php $bxs = wp_get_post_terms( $productID, 'pa_box-spring', array( 'order' => 'DESC', 'orderby' => 'name')); foreach ( $bxs as $bx ) { ?>
                              <div data-bx="<?php echo $bx->slug; ?>" class="button inline-block">
                                <?php echo $bx->name; ?>
                              </div>
                            <?php } ?>
                          </div>
                          <div class="step-4">
                            <div class="start-over left pt5 pr10 pointer"><i class="fa fa-arrow-circle-o-left whiteT" aria-hidden="true"></i></div>
                            <h4 class="capitalize pb5">You've selected <span></span> <span></span>, <span></span> box spring for <span class="bold"></span></h4>
                            <a href="" class="button"><i class="fa fa-check-circle-o usiGreenT" aria-hidden="true"></i> Begin Checkout</a>
                          </div>
													<div class="variations">
                            <?php $available_variations = $product->get_available_variations();
                              foreach ( $available_variations as $variation ) {
                          			echo '<div id="variation-',$variation['variation_id'],'" class="variation" data-vid="',$variation['variation_id'],'" data-vasize="',$variation['attributes']['attribute_pa_size'],'" data-vafeel="',$variation['attributes']['attribute_pa_feel'],'" data-vbox="',$variation['attributes']['attribute_pa_box-spring'],'" data-vprice="$',$variation['display_price'],'"></div>';
                          		}
                            ?>
                          </div>
                        </div>
					          	</div>
					          </div>
				          </div>
			        	</section>
			        </div>
						<?php	endwhile; } else {
							echo '<div class="w100 tac clearfix pt25">';
								echo __( 'No products found' );
							echo '</div>';
						}
						wp_reset_postdata();
					?>
				</div>
    	</div>
    </div>

  <?php endwhile; endif; ?>

<?php get_footer(); ?>