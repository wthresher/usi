<?php
// Register Theme Features
function usi_features()  {
	// Add theme support for Featured Images
	add_theme_support( 'post-thumbnails' );
	// Add theme support for HTML5 Semantic Markup
	add_theme_support( 'html5', array( 'search-form', 'gallery' ) );
	// Add theme support for document Title tag
	add_theme_support( 'title-tag' );
}

// Custom Wordpress Dashboard CSS
function ss_dashboard() { wp_enqueue_style('ss-dashboard-theme', get_template_directory_uri().'/_inc/css/dashboard.css'); }

// SVG Support
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

// Register Navigation Menus
function usi_menus() {
	$navigationMenuReg = array(
		'header' => __( 'Header Navigation', 'text_domain' ),
		'footer' => __( 'Footer Navigation', 'text_domain' )
	);
	register_nav_menus( $navigationMenuReg );
}

// Register Scripts
function usi_scripts() {
  if ( ! is_admin() ) {
  	wp_register_script( 'SiteJS', get_template_directory_uri().'/_inc/js/site.js', array( 'jquery' ), '1', true ); wp_enqueue_script( 'SiteJS' );
  	wp_register_script( 'Modernizr', get_template_directory_uri().'/_inc/dep/req/modernizr.js', null, '1', true ); wp_enqueue_script( 'Modernizr' );
  	wp_register_script( 'UA', get_template_directory_uri().'/_inc/dep/req/ua.js', null, '1', true ); wp_enqueue_script( 'UA' );
  	wp_register_script( 'FontAwesome', 'https://use.fontawesome.com/297ef15ab7.js', null, '1', true ); wp_enqueue_script( 'FontAwesome' );
  	wp_register_script( 'imagesLoaded', get_template_directory_uri().'/_inc/dep/req/imagesLoaded.pkgd.min.js', null, '1', true ); wp_enqueue_script( 'imagesLoaded' );
  	wp_register_script( 'Particles', get_template_directory_uri().'/_inc/dep/misc/particles.min.js', null, '2', true ); wp_enqueue_script( 'Particles' );
  	wp_register_script( 'ListJS', get_template_directory_uri().'/_inc/dep/misc/listjs.min.js', null, '2', true ); wp_enqueue_script( 'ListJS' );
  	wp_register_script( 'FocusPoint', get_template_directory_uri().'/_inc/dep/req/focuspoint.js', null, '2', true ); wp_enqueue_script( 'FocusPoint' );
  	wp_register_script( 'Vivus', get_template_directory_uri().'/_inc/dep/misc/vivus.min.js', null, '2', true ); wp_enqueue_script( 'Vivus' );
  	wp_register_script( 'Parallax', get_template_directory_uri().'/_inc/dep/misc/jquery.parallax.min.js', null, '2', true ); wp_enqueue_script( 'Parallax' );
  
    // GreenSock
  	wp_register_script( 'gsap', get_template_directory_uri().'/_inc/dep/greensock/minified/jquery.gsap.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap' );
  	wp_register_script( 'TweenLite', get_template_directory_uri().'/_inc/dep/greensock/minified/TweenLite.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'TweenLite' );
  	wp_register_script( 'gsap-timeline', get_template_directory_uri().'/_inc/dep/greensock/minified/TimelineLite.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap-timeline' );
  	wp_register_script( 'gsap-easing', get_template_directory_uri().'/_inc/dep/greensock/minified/easing/EasePack.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap-easing' );
  	wp_register_script( 'gsap-css', get_template_directory_uri().'/_inc/dep/greensock/minified/plugins/CSSPlugin.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap-css' );
  	wp_register_script( 'gsap-attr', get_template_directory_uri().'/_inc/dep/greensock/minified/plugins/AttrPlugin.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap-attr' );
  	wp_register_script( 'gsap-scrollTo', get_template_directory_uri().'/_inc/dep/greensock/minified/plugins/ScrollToPlugin.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap-scrollTo' );
  	wp_register_script( 'gsap-colorProps', get_template_directory_uri().'/_inc/dep/greensock/minified/plugins/ColorPropsPlugin.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap-colorProps' );
  	wp_register_script( 'gsap-cssrule', get_template_directory_uri().'/_inc/dep/greensock/minified/plugins/CSSRulePlugin.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap-cssrule' );
  	wp_register_script( 'gsap-easel', get_template_directory_uri().'/_inc/dep/greensock/minified/plugins/EaselPlugin.min.js', null, '1.18.2', true );
  	wp_enqueue_script( 'gsap-easel' );
  }
}

// Register Styles
function usi_styles() {
  if ( ! is_admin() ) {
    wp_register_style( 'WT', get_template_directory_uri().'/_inc/css/wt.css', false, '4.2.0', 'all' );
    wp_enqueue_style( 'WT' );
    wp_register_style( 'Main', get_template_directory_uri().'/_inc/css/main.css', false, '1.6', 'all' );
    wp_enqueue_style( 'Main' );
    wp_register_style( 'MQ', get_template_directory_uri().'/_inc/css/mq.css', false, '1.6', 'all' );
    wp_enqueue_style( 'MQ' );
  } else { }
}

// Register FAQs CPT
function usi_faqs() {

	$labels = array(
		'name'                  => _x( 'Inquiries', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Inquiry', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Inquiries', 'text_domain' ),
		'name_admin_bar'        => __( 'Inquiry', 'text_domain' ),
		'archives'              => __( 'Inquiry Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Inquiry Item:', 'text_domain' ),
		'all_items'             => __( 'All Inquiries', 'text_domain' ),
		'add_new_item'          => __( 'New Inquiry', 'text_domain' ),
		'add_new'               => __( 'New Inquiry', 'text_domain' ),
		'new_item'              => __( 'New Inquiry', 'text_domain' ),
		'edit_item'             => __( 'Edit Inquiry', 'text_domain' ),
		'update_item'           => __( 'Update Inquiry', 'text_domain' ),
		'view_item'             => __( 'View Inquiry', 'text_domain' ),
		'search_items'          => __( 'Search Inquiry', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Primary Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set primary image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove primary image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as primary image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Inquiry', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Inquiry', 'text_domain' ),
		'items_list'            => __( 'Inquiries list', 'text_domain' ),
		'items_list_navigation' => __( 'Inquiries list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Inquiries items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Inquiry', 'text_domain' ),
		'description'           => __( 'Our Inquiries', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'author', 'thumbnail', 'revisions', 'editor' ),
		'taxonomies'            => array( 'section' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-lightbulb',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'inquiry', $args );

}

// Register Section Taxonomy
function usi_tax_section() {

	$labels = array(
		'name'                       => _x( 'Section', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Section', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Sections', 'text_domain' ),
		'all_items'                  => __( 'All Sections', 'text_domain' ),
		'parent_item'                => __( 'Parent Section Category', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Section Category:', 'text_domain' ),
		'new_item_name'              => __( 'New Section', 'text_domain' ),
		'add_new_item'               => __( 'Add New Section', 'text_domain' ),
		'edit_item'                  => __( 'Edit Section', 'text_domain' ),
		'update_item'                => __( 'Update Section', 'text_domain' ),
		'view_item'                  => __( 'View Section', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate Sections with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove Sections', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Sections', 'text_domain' ),
		'search_items'               => __( 'Search Sections', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No Section', 'text_domain' ),
		'items_list'                 => __( 'Sections list', 'text_domain' ),
		'items_list_navigation'      => __( 'Sections list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'section', array( 'inquiry' ), $args );

}

// Register Reviews CPT
function usi_reviews() {

	$labels = array(
		'name'                  => _x( 'Reviews', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Review', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Reviews', 'text_domain' ),
		'name_admin_bar'        => __( 'Review', 'text_domain' ),
		'archives'              => __( 'Review Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Review Item:', 'text_domain' ),
		'all_items'             => __( 'All Reviews', 'text_domain' ),
		'add_new_item'          => __( 'Add New Review', 'text_domain' ),
		'add_new'               => __( 'Add New Review', 'text_domain' ),
		'new_item'              => __( 'Add New Review', 'text_domain' ),
		'edit_item'             => __( 'Edit Review', 'text_domain' ),
		'update_item'           => __( 'Update Review', 'text_domain' ),
		'view_item'             => __( 'View Review', 'text_domain' ),
		'search_items'          => __( 'Search Review', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Primary Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set primary image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove primary image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as primary image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Review', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Review', 'text_domain' ),
		'items_list'            => __( 'Reviews list', 'text_domain' ),
		'items_list_navigation' => __( 'Reviews list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Review items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Review', 'text_domain' ),
		'description'           => __( 'Our Reviews', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'revisions' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-heart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'review', $args );

}

// Add ACF Options Page
if( function_exists('acf_add_options_page') ) {
  $option_page = acf_add_options_page(array(
  	'page_title' 	=> 'Set Global Unicorn Sleep Options',
  	'menu_title' 	=> 'USI',
  	'menu_slug' 	=> 'usi-info',
  	'icon_url'    => 'dashicons-marker'
  ));
}

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  echo '<section id="main">';
}

function my_theme_wrapper_end() {
  echo '</section>';
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Remove Certain Menu Items
/*
function usi_menu_edit(){
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'jetpack' );                    //Jetpack*
  remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'themes.php' );                 //Appearance
  // remove_menu_page( 'plugins.php' );                //Plugins
  // remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings
}
*/

/**
 * Optimize WooCommerce Scripts
 * Remove WooCommerce Generator tag, styles, and scripts from non WooCommerce pages.
 */
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );

function child_manage_woocommerce_styles() {
	//remove generator meta tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

	//first check that woo exists to prevent fatal errors
	if ( function_exists( 'is_woocommerce' ) ) {
		//dequeue scripts and styles
		if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
			wp_dequeue_style( 'woocommerce_frontend_styles' );
			wp_dequeue_style( 'woocommerce_fancybox_styles' );
			wp_dequeue_style( 'woocommerce_chosen_styles' );
			wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
			wp_dequeue_script( 'wc_price_slider' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-add-to-cart' );
			wp_dequeue_script( 'wc-cart-fragments' );
			wp_dequeue_script( 'wc-checkout' );
			wp_dequeue_script( 'wc-add-to-cart-variation' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-cart' );
			wp_dequeue_script( 'wc-chosen' );
			wp_dequeue_script( 'woocommerce' );
			wp_dequeue_script( 'prettyPhoto' );
			wp_dequeue_script( 'prettyPhoto-init' );
			wp_dequeue_script( 'jquery-blockui' );
			wp_dequeue_script( 'jquery-placeholder' );
			wp_dequeue_script( 'fancybox' );
			wp_dequeue_script( 'jqueryui' );
      wp_dequeue_style( 'woocommerce-general' );
      wp_dequeue_style( 'woocommerce-layout' );
      wp_dequeue_style( 'woocommerce-smallscreen' );
		}
	}

}

// Remove WooCommerce Tabs
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab
    return $tabs;
}

// Disbale Certain Scripts
function disable_wp_emojicons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

add_filter( 'woocommerce_checkout_fields', 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {

	// Special Exceptions
	$exceptions = array(
		'billing_address_1' => 'Street Address',
		'billing_address_2' => 'Apartment, suite, unit etc. (optional)',
		'shipping_address_1' => 'Street Address',
		'shipping_address_2' => 'Apartment, suite, unit etc. (optional)'
	);

	foreach ( $fields as &$field_section ) {
		foreach ( $field_section as $field_name => &$field ) {

			// Check for exceptions
			if ( array_key_exists( $field_name, $exceptions ) ) {
				$field['placeholder'] = $exceptions[$field_name];
			}
			else {
				$field['placeholder'] = $field['label'];
			}

		}
	}

	return $fields;
}

// Hooks
add_action( 'init', 'usi_menus', 0 ); // Add Menus
add_action( 'init', 'usi_scripts' ); // Add Scripts
add_action( 'init', 'usi_styles', 0 ); // Add Styles
add_action( 'init', 'usi_faqs', 0 ); // Register FAQs Post Type
add_action( 'init', 'usi_tax_section', 0 ); // Register Type Taxonomy
add_action( 'init', 'usi_reviews', 0 ); // Register FAQs Post Type
add_action( 'admin_menu', 'usi_menu_edit' ); // Remove Menu Items
add_action( 'wp_enqueue_scripts', 'usi_scripts' ); // Enqueue Scripts
add_action( 'wp_enqueue_scripts', 'usi_styles' ); // Enqueue Styles
add_action( 'after_setup_theme', 'usi_features' ); // Theme Features
add_action( 'admin_enqueue_scripts', 'ss_dashboard' ); // Add custom dashboard styles
add_action( 'login_enqueue_scripts', 'ss_dashboard' ); // Add custom login styles
add_action( 'init', 'disable_wp_emojicons' ); // Remove Certain Scripts
add_filter( 'emoji_svg_url', '__return_false' );
add_filter( 'show_admin_bar', '__return_false' ); // Disable WP Admin Bar
add_filter( 'wpseo_metabox_prio', function() { return 'low';}); // Filter Yoast Meta Priority
add_filter( 'upload_mimes', 'cc_mime_types' ); // Add SVG Support
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);