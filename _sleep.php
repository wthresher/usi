<?php get_header(); /* Template Name: Sleep */ ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();?>

    <div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page pt35 pb55 zDex">
			<h1 class="tac pb20 gFadeIn">The Finest Materials<br />Provide The Best Sleep</h1>
			<div class="w100 clearfix">
				<div class="w50 left pr55">
					<ul>
						<li class="gFadeInD1">
							<h4 class="m0a pb5">Joma Wool</h4>
							<p>
								Natural wool is the perfect insulator. Having it right under the fabric is for good moisture and temperature control ensuring a healthy sleeping surface.
							</p>
						</li>
						<li class="gFadeInD1">
							<h4 class="m0a pb5">Nano Coil</h4>
							<p>
								The tiny nano coils add a buoyant layer near the surface of the mattress, which provides delicate contouring to cradle your body as your rest.
							</p>
						</li>
						<li class="gFadeInD1">
							<h4 class="m0a pb5">High Density Convoluted Foam</h4>
							<p>
								A comfortable and plush quilt with high-density foam to keep it feeling great year after year.
							</p>
						</li>
					</ul>
				</div>
				<div class="w50 left">
					<ul>
						<li class="gFadeInD1">
							<h4 class="m0a pb5">Polyester & Rayon FR Barrier</h4>
							<p>
								Non-allergenic polyester and rayon from natural sources provide a protective barrier with no chemical additives.
							</p>
						</li>
						<li class="gFadeInD1">
							<h4 class="m0a pb5">High Resiliency Foam</h4>
							<p>
								The highest grade of polyurethane foam for durability and support for years of use without body impressions.
							</p>
						</li>
						<li class="gFadeInD1">
							<h4 class="m0a pb5">Hi-Loft Joma Wool Blanket</h4>
							<p>
								Each Joma wool fiber is individually crimped to act like a little spring. The 1" thick blanket provides a natural and supportive base for the comfort layers of the mattress.
							</p>
						</li>
					</ul>
				</div>
			</div>
    </div>

  <?php endwhile; endif; ?>

<?php get_footer(); ?>