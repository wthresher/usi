<?php get_header(); /* Template Name: Shop */ ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();?>
		<div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page">

 <div id="features" class="w100 clearfix">

      	<!-- Sleep in Luxury -->
        <div class="w100 clearfix">
        	<section>
	          <div class="w50 left pt50">
							<div id="maxx-home" data-parallax='{"y":-15,"x":20,"duration":1000}' class="dropshadow relative">
	        			<img src="<?php echo get_template_directory_uri(); ?>/_inc/img/Rever-Corner-Left-DS@2x.png" class="gFadeInD2 w75 m0a relative layer1" />
	        		</div>
	          </div>
	          <div class="wrapper-content">
		          <div class="w50 left pt95">
		          	<div data-parallax='{"y":20,"x":-70,"duration":1500}'>
			          	<h2>Sleep in Luxury</h2>
			            <p class="pb15">
			              Years of research complemented with the highest quality materials makes Unicorn Sleep mattresses the most advanced in the world. Delivered and set up for FREE courtesy of our white glove treatment. Everyone deserves luxurious sleep.
			            </p>
			            <a href="" class="button">Learn More About Unicorn Sleep</a>
		          	</div>
		          </div>
	          </div>
        	</section>
        </div>

				<!-- Invest In Your Health -->
				<div class="w100 clearfix pt100">
					<section>
						<div class="wrapper-content">
		          <div class="w50 left pr25">
		          	<div data-parallax='{"x":20,"duration":500}'>
			          	<h2>Invest In Your Health</h2>
			            <p>
			              Quality sleep improves sex life, chronic pain, weight control, mood, memory, immunity and lowers risk of potential cardiac problems. Unicorn Sleep mattresses were designed from the ground up with health benefits in mind.
			            </p>
			            <a href="" class="button">Find Out How</a>
		          	</div>
		          </div>
	          </div>
	          <div class="w50 left pt50 ohidden">
							<div id="invest-home" data-parallax='{"y":-20,"x":-20,"duration":5000}'>
	        			<img src="<?php echo get_template_directory_uri(); ?>/_inc/img/Franklin-Right@2x.png" class="gFadeInD2 relative right" />
	        		</div>
	          </div>
					</section>
        </div>

				<!-- Handmade in USA -->
				<div class="w100 clearfix pt100">
					<section>
	          <div class="w50 left">
	          	<div id="handmade-home" data-parallax='{"y":-50,"x":-10,"duration":5000}'>
	        			<img src="<?php echo get_template_directory_uri(); ?>/_inc/img/MaxxCut@2x.png" class="gFadeInD2 relative w90" />
	        		</div>
	          </div>
	          <div class="wrapper-content">
		          <div class="w50 left pt65">
		          	<div data-parallax='{"x":-25,"duration":1500}'>
			          	<h2>Handmade in the USA</h2>
			            <p>
			              Each Unicorn Sleep mattresses is handmade in California by master craftsmen who are passionate about providing the best sleep of your life.
			            </p>
			            <a href="" class="button">See What Sets Us Above</a>
		          	</div>
		          </div>
	          </div>
					</section>
        </div>


<!--
  		<?php $args = array( 'post_type' => 'product', 'posts_per_page' => 10 ); $loop = new WP_Query( $args ); while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
        <a href="<?php get_permalink() ?>">
          <?php echo woocommerce_get_product_thumbnail(); ?>
          <?php echo get_the_title(); ?>
          <?php woocommerce_template_loop_add_to_cart(); ?>
        </a>
  		<?php	endwhile; wp_reset_query(); ?>
-->

    </div>
  <?php endwhile; endif; ?>

<?php get_footer(); ?>