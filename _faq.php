<?php get_header(); /* Template Name: FAQs */ ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();?>
		<div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page pt15">
			<div class="wrapper-content">
				<h1 class="tac pb20 gFadeIn">Frequently Asked Questions</h1>
	      <div class="border-boxes w100 clearfix">
	        <div class="faq-section-box w33 left tac pointer" data-targets="features">
	          <div class="gFadeInD1">
	            <img src="<?php echo get_template_directory_uri(); ?>/_inc/img/Diamond.svg" width="70"></img>
	            <h3>Features</h3>
	          </div>
	        </div>
	        <div class="faq-section-box w33 left tac pointer" data-targets="shipping-arrival">
	          <div class="gFadeInD2">
	            <img src="<?php echo get_template_directory_uri(); ?>/_inc/img/Truck.svg" width="70"></img>
	            <h3>Shipping & Arrival</h3>
	          </div>
	        </div>
	        <div class="faq-section-box w33 left tac pointer" data-targets="importance-of-sleep">
	          <div class="gFadeInD3">
	            <img src="<?php echo get_template_directory_uri(); ?>/_inc/img/Cloud-Line.svg" width="70"></img>
	            <h3>Importance of Sleep</h3>
	          </div>
	        </div>
	      </div>
	      <div id="faqs" class="w100 clearfix pt25 tac">
	        <div class="w100 clearfix gFadeInD4">
	          <input class="search big-search tac w100" placeholder="Begin typing your question..." />
	        </div>
	        <ul id="faqs-list" class="w100 m0a clearfix border-list list gFadeInD5">
	          <?php
	            $args = array (
	            	'post_type'              => 'faq',
	            	'order'                  => 'DESC',
	            	'orderby'                => 'menu_order',
	            	'cache_results'          => true,
	            );
	            $q = new WP_Query( $args ); if ( $q->have_posts() ) { while ( $q->have_posts() ) { $q->the_post(); ?>
	              <li class="transitionDefault <?php $terms = get_the_terms( $post->ID , 'section' ); foreach ( $terms as $term ) { echo $term->slug; echo " ";  }?>">
	                <h4 id="faq-title-<?php the_id(); ?>" class="faq-title m0a p15 workSans"><?php the_title(); ?></h4>
	                <div id="faq-content-<?php the_id(); ?>" class="faq-content w100 clearfix p10">
	                  <?php the_content(); ?>
	                </div>
	              </li>
	          <?php	} } else { } wp_reset_postdata(); ?>
	        </ul>
	      </div>
			</div>
    </div>
  <?php endwhile; endif; ?>

<?php get_footer(); ?>