<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?> class="no-js <?php if (is_page_template('_home.php')){ ?>home<?php } else {} ?>">
<!--<![endif]-->

<head profile="http://www.w3.org/2005/10/profile">

  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
  <meta name="robots" content="index,follow">
  <meta name="google-site-verification" content="qEPJFjhZKVi6nfDt2SseqEaMvSQPRWbWmhHzmQzCSK4" />
  <title><?php wp_title( '|', true, 'right' ); ?></title>

  <?php wp_enqueue_script('jquery'); ?>
  <?php wp_head(); ?>

  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196" />
  <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="/favicon-128.png" sizes="128x128" />
  <meta name="application-name" content="&nbsp;"/>
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="mstile-144x144.png" />
  <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
  <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
  <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
  <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

	<div class="dnone">
		<?php $bclasses = get_body_class(); $values = get_field( 'header_pixel', $post->ID ); if ( $values ) { ?>
			<?php the_field('header_pixel', $post->ID); ?>
		<?php } else if (in_array('woocommerce-order-received', $bclasses)) { ?>
  		<script src="https://secure.adnxs.com/px?id=802445&order_id=[ORDER_ID]&value=[REVENUE]&t=1" type="text/javascript"></script>
  		<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=561078567431646&ev=purchase&cd[currency]=USD&cd[value]=0.00"/>
  		<img height='1' width='1' border='0' alt='' src='https://s.amazon-adsystem.com/iui3?d=forester-did&ex-fargs=%3Fid%3Dc4329379-ab5c-733d-492c-03bc992d0f5c%26type%3D54%26m%3D1&ex-fch=416613&ex-src=www.unicornsleep.com&ex-hargs=v%3D1.0%3Bc%3D1762006620501%3Bp%3DC4329379-AB5C-733D-492C-03BC992D0F5C' />
    <?php } else {} ?>
	</div>

</head>

<body <?php body_class(); ?>>

  <!-- Google Analytics -->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  
    ga('create', 'UA-90925317-1', 'auto');
    ga('send', 'pageview');
  </script>

  <!-- Insert Pixels for KY -->
	<div class="dnone">
		<?php $values = get_field( 'body_pixel', $post->ID ); if ( $values ) { ?>
				<?php the_field('body_pixel', $post->ID); ?>
		<?php } else if (in_array('woocommerce-order-received', $bclasses)) { ?>
      <img src="https://ad.doubleclick.net/ddm/activity/src=6416901;type=sales;cat=msv7npog;qty=[Quantity];cost=[Revenue];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=[OrderID]?" width="1" height="1" alt=""/>
    <?php } else {} ?>
	</div>

  <header id="header-site">
    <div class="wrapper-site zDex">
      <div class="w100 h100 table">
				<div class="absolute r0">
          <ul id="actions" class="menu-inline inline-block pt10">
            <li class="pl10">
              <div class="modal-trigger pointer" rel="chat">
                <img src="<?php echo get_template_directory_uri(); ?>/_inc/img/icon-email.svg" alt="Contact Unicorn Sleep" />
              </div>
            </li>
            <li class="pl10">
              <a href="<?php echo home_url(); ?>/cart">
                <img src="<?php echo get_template_directory_uri(); ?>/_inc/img/Cart-W.svg" width="12" />
              </a>
            </li>
          </ul>
				</div>
				<div id="main-navigation" class="tableRow h100">
          <div class="tableCell vam tac pt20 mobile-show">
            <a href="<?php echo home_url(); ?>" class="block logo">
              <img src="<?php echo get_template_directory_uri(); ?>/_inc/img/Unicorn-Sleep-Emblem.svg"
              onerror="this.src='@2x.png'; this.onerror=null;"
              width="50" height="auto" alt="Unicorn Sleep" />
            </a>
          </div>
					<div class="tableCell h100 vam tac pt25">
            <nav role="navigation" class="w100 menu-inline lessBlur">
							<ul>
								<li id="nav-vintage" class="w33 left">
									<a href="<?php echo home_url(); ?>/vintage">Vintage</a>
								</li>
								<li id="nav-natural" class="w33 left">
									<a href="<?php echo home_url(); ?>/natural">Natural</a>
								</li>
								<li id="nav-modern" class="w33 left">
									<a href="<?php echo home_url(); ?>/modern">Modern</a>
								</li>
							</ul>
            </nav>
          </div>
          <div class="tableCell vam tac pt20 mobile-hide">
            <a href="<?php echo home_url(); ?>" class="block logo">
              <img src="<?php echo get_template_directory_uri(); ?>/_inc/img/Unicorn-Sleep-Emblem.svg"
              onerror="this.src='@2x.png'; this.onerror=null;"
              width="50" height="auto" alt="Unicorn Sleep" />
            </a>
          </div>
					<div class="tableCell h100 vam tac pt20">
            <nav role="navigation" class="w100 menu-inline">
							<ul>
								<li id="nav-experience" class="w33 left">
									<a href="<?php echo home_url(); ?>/experience">Experience</a>
								</li>
								<li id="nav-us" class="w33 left">
									<a href="<?php echo home_url(); ?>/us">Us</a>
								</li>
								<li id="nav-inquiries" class="w33 left">
									<a href="<?php echo home_url(); ?>/inquiries">Inquiries</a>
								</li>
							</ul>
            </nav>
          </div>
				</div>
      </div>
    </div>
  </header>

	<div class="wrapper-main zDex">