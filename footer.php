  </div>

  <footer id="footer-site" class="clearfix zDex">
    <div class="wrapper-site h100">
      <div class="w100 h100 table pt30">
        <div class="tableCell vat w20">
          <h5 class="uppercase pb10">Unicorn Sleep Incorporated</h5>
          <div class="w100 clearfix">
            <a href="mailto:info@unicornsleep.com"><img src="<?php echo get_template_directory_uri(); ?>/_inc/img/icon-email.svg" alt="Call Unicorn Sleep" /> info@unicornsleep.com</a>
            <div class="w100 clearfix pt10 pb10">
              <address>
                New York NY, USA
              </address>
            </div>
            &copy; <?php echo date('Y'); ?>
          </div>
        </div>
        <div class="tableCell vat w5 mobile-hide">
          <h5 class="uppercase pb10">Menu</h5>
          <?php wp_nav_menu( array('menu' => 'footer', 'container' => '', 'menu_id' => 'menu-footer' )); ?>
        </div>
        <div class="tableCell vat w5 mobile-hide">
          <h5 class="uppercase pb10">&nbsp;</h5>
          <ul>
            <li><a href="<?php echo home_url(); ?>/cart">Cart</a></li>
            <li>
              <?php if ( is_user_logged_in() ) { ?>
                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Account','woothemes'); ?>"><?php _e('Account','woothemes'); ?></a>
              <?php } else { ?>
                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login','woothemes'); ?>"><?php _e('Login','woothemes'); ?></a>
              <?php } ?>
            </li>
            <li><div class="modal-trigger pointer" rel="chat">Contact</div></li>
          </ul>
        </div>
        <div class="tableCell vat w5 mobile-hide">
          <h5 class="uppercase pb10">&nbsp;</h5>
          <ul class="pl20">
            <li><a href="<?php echo home_url(); ?>/returns">Returns</a></li>
            <li><a href="<?php echo home_url(); ?>/warranty">Warranty</a></li>
            <li><a href="<?php echo home_url(); ?>/privacy-policy">Privacy</a></li>
          </ul>
        </div>
        <div class="tableCell vat w20">
          <div class="block right">
            <h5 class="uppercase pb10 pl10 inline-block left">Connect With Us</h5>
            <div class="inline-block pb10 pl10 left">
	          	<ul class="menu-inline">
	              <li class="pr5 pl5"><a href="https://www.facebook.com/UnicornSleep" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/_inc/img/icon-facebook.svg" alt="Unicorn Sleep Facebook" width="15" /></a></li>
	              <li class="pr5 pl5"><a href="https://www.instagram.com/unicornsleepllc/"><img src="<?php echo get_template_directory_uri(); ?>/_inc/img/icon-instagram.svg" alt="Unicorn Sleep Instagram" width="15" /></a></li>
	            </ul>
            </div>
            <div class="w100 clearfix pl10 mobile-hide">
              <!-- Begin MailChimp Signup Form -->
              <div id="mc_embed_signup">
              <form action="//unicornlseep.us15.list-manage.com/subscribe/post?u=edbf10d4fcd28c5a5ca0dd262&amp;id=658eaa7b48" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                  <div id="mc_embed_signup_scroll">

              <div class="mc-field-group">
              	<input type="email" value="" name="EMAIL" class="required email inline-block left" id="mce-EMAIL" placeholder="Enter Your Email...">
              	<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="inline-block left whiteT">
              </div>
              	<div id="mce-responses" class="clear">
              		<div class="response" id="mce-error-response" style="display:none"></div>
              		<div class="response" id="mce-success-response" style="display:none"></div>
              	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_edbf10d4fcd28c5a5ca0dd262_658eaa7b48" tabindex="-1" value=""></div>
                  </div>
              </form>
              </div>
              <!--End mc_embed_signup-->
            </div>
            <div class="w100 clearfix pt20 pl10">
							<a href="http://sharkshark.co" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/_inc/img/SharkShark-Logo@2x.png"
								width="70" height="auto" alt="Website by: SharkShark" />
							</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <div class="parallax-particles ohidden fixed t0 l0 w100 h100">
    <div id="particles" class="layer" data-depth="1"></div>
  </div>

  <!-- Contact Modal -->
  <div id="chat" class="modal animationHide transitionDefault">
		<div>
			<h2 class="m0a pb10 w100 tac">Send Us a Message</h2>
			<div class="w100 tac">
  			<span>Feel free to email us directly at: </span>
  			<a href="mailto:info@unicornsleep.com"><img src="<?php echo get_template_directory_uri(); ?>/_inc/img/icon-email-purple.svg" alt="Call Unicorn Sleep" /> info@unicornsleep.com</a>
			</div>
	    <div class="w100 clearfix pt10">
  	    <?php echo do_shortcode('[contact-form-7 id="90" title="Contact"]'); ?>
	    </div>
	  </div>
  </div>

	<div class="modal-overlay animationHide transitionDefault"></div>

  <?php wp_footer(); ?>

</body>

</html>