<?php get_header(); /* Template Name: Reviews */ ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();?>

    <div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page pt35 pb55 zDex">
      <h1 class="tac pb20 gFadeIn">What our customers are saying</h1>
  	  <div class="w100 clearfix">
  	    <?php
  	      $args = array (
  	      	'post_type'              => 'review',
  	      	'order'                  => 'DESC',
  	      	'orderby'                => 'date',
  	      	'cache_results'          => true,
  	      );
  	      $q = new WP_Query( $args ); if ( $q->have_posts() ) { while ( $q->have_posts() ) { $q->the_post(); ?>
            <div class="w70 m0a tac borderBottom">
              <div class="gFadeInD1 pr25 pl25 pb25 pt15">
                <p>
    	      			<?php the_field('customer_review'); ?>
    	      		</p>
                <h3><?php the_title(); ?></h3>
              </div>
            </div>
  	      <?php	} } else { } wp_reset_postdata(); ?>
  	  </div>
    </div>

  <?php endwhile; endif; ?>

<?php get_footer(); ?>