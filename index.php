<?php get_header(); ?>
  
  <?php if (have_posts()) : while (have_posts()) : the_post();?>
  
    <div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page">
      <?php the_content(); ?>
    </div>
  
  <?php endwhile; endif; ?>

<?php get_footer(); ?>