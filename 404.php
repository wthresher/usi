<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();?>

    <div id="page-<?php echo basename(get_permalink()); ?>" class="wrapper-page">
    	<div class="w100 clearfix wrapper-content tac">
    		<h1>Whoops! That didn't work!</h1>
    		<a href="<?php echo home_url(); ?>" class="button">Go Home</a>
    	</div>
    </div>

  <?php endwhile; endif; ?>

<?php get_footer(); ?>